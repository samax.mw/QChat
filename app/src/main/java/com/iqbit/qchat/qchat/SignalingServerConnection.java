package com.iqbit.qchat.qchat;

import android.os.AsyncTask;
import android.util.Log;

import com.neovisionaries.ws.client.ThreadType;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.neovisionaries.ws.client.WebSocketListener;
import com.neovisionaries.ws.client.WebSocketState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class SignalingServerConnection {
    private static class SocketTaskRunner extends AsyncTask<WebSocket, Void, Void> {
        private OnConnectErrorListener onConnectErrorListener;

        protected Void doInBackground(WebSocket... sockets) {
            WebSocket serverConnection = sockets[0];
            try {
                serverConnection.connect();
            } catch (WebSocketException ex) {
                onConnectErrorListener.onConnectError(serverConnection, ex);
            }
            return null;
        }
    }

    private WebSocket socket = null;

    interface OnConnectedListener {
        void onConnected(WebSocket websocket, Map<String, List<String>> headers);
    }

    interface OnConnectErrorListener {
        void onConnectError(WebSocket websocket, WebSocketException cause);
    }

    interface OnDisconnectListener {
        void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer);
    }

    interface OnTextMessageListener {
        void onTextMessage(WebSocket websocket, String text);
    }

    interface OnTextMessageSentListener {
        void onTextMessageSent(WebSocket websocket, String text);
    }

    interface OnTextMessageSendErrorListener {
        void onTextMessageSendError(WebSocket websocket, WebSocketException cause, String text);
    }

    private OnConnectedListener onConnectedListener;
    private ArrayList<OnConnectedListener> onConnectedListeners = new ArrayList<>();

    private OnConnectErrorListener onConnectErrorListener;
    private ArrayList<OnConnectErrorListener> onConnectErrorListeners = new ArrayList<>();

    private OnDisconnectListener onDisconnectListener;
    private ArrayList<OnDisconnectListener> onDisconnectListeners = new ArrayList<>();

    private OnTextMessageListener onTextMessageListener;
    private ArrayList<OnTextMessageListener> onTextMessageListeners = new ArrayList<>();

    private OnTextMessageSentListener onTextMessageSentListener;
    private ArrayList<OnTextMessageSentListener> onTextMessageSentListeners = new ArrayList<>();

    private OnTextMessageSendErrorListener onTextMessageSendErrorListener;
    private ArrayList<OnTextMessageSendErrorListener> onTextMessageSendErrorListeners = new ArrayList<>();

    public SignalingServerConnection(String uri) {
        WebSocketFactory webSocketFactory = new WebSocketFactory();
        webSocketFactory.setConnectionTimeout(10000);

        try {
            socket = webSocketFactory.createSocket(uri/*"ws://192.168.1.3:8083/" + userId*/);
            socket.addListener(new WebSocketListener() {
                @Override
                public void onStateChanged(WebSocket websocket, WebSocketState newState) throws Exception {

                }

                @Override
                public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
                    if (onConnectedListener != null) {
                        onConnectedListener.onConnected(websocket, headers);
                        onConnectedListener = null;
                    }
                    for (OnConnectedListener listener : onConnectedListeners) listener.onConnected(websocket, headers);
                }

                @Override
                public void onConnectError(WebSocket websocket, WebSocketException cause) throws Exception {
                    if (onConnectErrorListener != null) {
                        onConnectErrorListener.onConnectError(websocket, cause);
                        onConnectErrorListener = null;
                    }
                    for (OnConnectErrorListener listener : onConnectErrorListeners) listener.onConnectError(websocket, cause);
                }

                @Override
                public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
                    if (onDisconnectListener != null) {
                        onDisconnectListener.onDisconnected(websocket, serverCloseFrame, clientCloseFrame, closedByServer);
                        onDisconnectListener = null;
                    }
                    for (OnDisconnectListener listener : onDisconnectListeners) listener.onDisconnected(websocket, serverCloseFrame, clientCloseFrame, closedByServer);
                }

                @Override
                public void onFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onContinuationFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onTextFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onBinaryFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onCloseFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onPingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {}

                @Override
                public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onTextMessage(WebSocket websocket, String text) throws Exception {
                    if (onTextMessageListener != null) {
                        onTextMessageListener.onTextMessage(websocket, text);
                        onTextMessageListener = null;
                    }
                    for (OnTextMessageListener listener : onTextMessageListeners) listener.onTextMessage(websocket, text);
                }

                @Override
                public void onBinaryMessage(WebSocket websocket, byte[] binary) throws Exception {

                }

                @Override
                public void onSendingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onFrameSent(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    if (frame.isTextFrame() && onTextMessageSentListener != null) {
                        onTextMessageSentListener.onTextMessageSent(websocket, frame.getPayloadText());
                        onTextMessageSentListener = null;
                    }
                    for (OnTextMessageSentListener listener : onTextMessageSentListeners) listener.onTextMessageSent(websocket, frame.getPayloadText());
                }

                @Override
                public void onFrameUnsent(WebSocket websocket, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onThreadCreated(WebSocket websocket, ThreadType threadType, Thread thread) throws Exception {

                }

                @Override
                public void onThreadStarted(WebSocket websocket, ThreadType threadType, Thread thread) throws Exception {

                }

                @Override
                public void onThreadStopping(WebSocket websocket, ThreadType threadType, Thread thread) throws Exception {

                }

                @Override
                public void onError(WebSocket websocket, WebSocketException cause) throws Exception {

                }

                @Override
                public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {

                }

                @Override
                public void onMessageError(WebSocket websocket, WebSocketException cause, List<WebSocketFrame> frames) throws Exception {

                }

                @Override
                public void onMessageDecompressionError(WebSocket websocket, WebSocketException cause, byte[] compressed) throws Exception {

                }

                @Override
                public void onTextMessageError(WebSocket websocket, WebSocketException cause, byte[] data) throws Exception {

                }

                @Override
                public void onSendError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {
                    if (/*frame.isTextFrame() && */onTextMessageSendErrorListener != null) {
                        onTextMessageSendErrorListener.onTextMessageSendError(websocket, cause, frame != null ? frame.getPayloadText() : "");
                        onTextMessageSendErrorListener = null;
                    }
                    for (OnTextMessageSendErrorListener listener : onTextMessageSendErrorListeners) listener.onTextMessageSendError(websocket, cause, frame != null ? frame.getPayloadText() : "");
                }

                @Override
                public void onUnexpectedError(WebSocket websocket, WebSocketException cause) throws Exception {

                }

                @Override
                public void handleCallbackError(WebSocket websocket, Throwable cause) throws Exception {

                }

                @Override
                public void onSendingHandshake(WebSocket websocket, String requestLine, List<String[]> headers) throws Exception {

                }
            });
        } catch (Exception ex) {
            try {
                socket = webSocketFactory.createSocket("ws://host-uir.invalid");
            } catch (Exception _ex) {
                Log.d("no_exception_failed", "err: " + _ex.getMessage());
            }
        }
    }

    public void sendText(String message) {
        socket.sendText(message);
    }

    public void sendText(String message, OnTextMessageSentListener onTextSent) {
        onTextMessageSentListener = onTextSent;
        sendText(message);
    }

    public void sendText(String message, OnTextMessageSentListener onTextSent, OnTextMessageSendErrorListener onTextSendError) {
        onTextMessageSentListener = onTextSent;
        onTextMessageSendErrorListener = onTextSendError;
        sendText(message);
    }

    public void connect(OnConnectedListener onConnected, OnConnectErrorListener onConnectError) {
        onConnectedListener = onConnected;
        onConnectErrorListener = onConnectError;

        SocketTaskRunner taskRunner = new SocketTaskRunner();
        taskRunner.onConnectErrorListener = onConnectErrorListener;
        taskRunner.execute(socket);
    }

    public void addOnTextMessageListener(OnTextMessageListener listener) {
        onTextMessageListeners.add(listener);
    }
}
