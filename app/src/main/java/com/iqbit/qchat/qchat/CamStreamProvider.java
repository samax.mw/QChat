package com.iqbit.qchat.qchat;

import android.content.Context;
import android.util.Log;

import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera1Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.CapturerObserver;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;


public class CamStreamProvider {
    private Context context;
    
    CamStreamProvider(Context context) {
        this.context = context;
    }
    
    private String getFrontCameraId() {
        CameraEnumerator cameraEnumerator = new Camera1Enumerator();
        String cameraId[] = cameraEnumerator.getDeviceNames();
        
        for (String id : cameraId)
            if (cameraEnumerator.isFrontFacing(id))
                return id;

        return "";
    }

    public Context getContext() {
        return context;
    }
    
    public MediaStream getCamStream(PeerConnectionFactory factory) {
        VideoSource videoSource = factory.createVideoSource(false);
        VideoTrack videoTrack = factory.createVideoTrack("VT-0001", videoSource);
        startCameraCapture(videoSource.getCapturerObserver());

        AudioSource audioSource = factory.createAudioSource(new MediaConstraints());
        AudioTrack audioTrack = factory.createAudioTrack("AT-0001", audioSource);

        MediaStream mediaStream = factory.createLocalMediaStream("MS-20");
        mediaStream.addTrack(videoTrack);
        mediaStream.addTrack(audioTrack);

        return mediaStream;
    }

    private void startCameraCapture(CapturerObserver capturerObserver) {
        CameraVideoCapturer capturer = (new Camera1Enumerator()).createCapturer(
            getFrontCameraId(),
            new CameraVideoCapturer.CameraEventsHandler() {
                @Override
                public void onCameraError(String s) {
                    Log.d("onCameraError", s);
                }

                @Override
                public void onCameraDisconnected() {
                    Log.d("onCameraDisconnected", "Normal disconnect");
                }

                @Override
                public void onCameraFreezed(String s) {
                    Log.d("onCameraFreezed", s);
                }

                @Override
                public void onCameraOpening(String s) {
                    Log.d("onCameraOpening", s);
                }

                @Override
                public void onFirstFrameAvailable() {
                    Log.d("onFirstFrameAvailable", "got camera frame. yay");
                }

                @Override
                public void onCameraClosed() {
                    Log.d("onCameraClosed", "Normal close");
                }
            }
        );

        capturer.initialize(SurfaceTextureHelper.create(
            "csp_thread", EglContext.get()),
            context,
            new CapturerObserver() {
                @Override
                public void onCapturerStarted(boolean b) {
                    capturerObserver.onCapturerStarted(b);
                }

                @Override
                public void onCapturerStopped() {
                    capturerObserver.onCapturerStopped();
                }

                @Override
                public void onFrameCaptured(VideoFrame videoFrame) {
                    capturerObserver.onFrameCaptured(videoFrame);
                }
            }
        );

        capturer.startCapture(640, 480, 30);
    }
}
