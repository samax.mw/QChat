package com.iqbit.qchat.qchat;

import android.content.Context;


public class SignalingServerURIProvider extends SettingsProvider {
    public static final String KEY_SERVER_URI = "ServerURI";

    SignalingServerURIProvider(Context context) {
        super(context);
    }

    public String getServerURI() {
        return getPrefValue(KEY_SERVER_URI);
    }

    public void setServerURI(String address) {
        setPrefValue(KEY_SERVER_URI, address);
    }
}
