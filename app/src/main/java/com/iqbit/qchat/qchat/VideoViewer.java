package com.iqbit.qchat.qchat;

import android.support.annotation.NonNull;

import org.webrtc.SurfaceViewRenderer;


public class VideoViewer {
    private SurfaceViewRenderer videoViewer;

    VideoViewer(@NonNull SurfaceViewRenderer videoViewer) {
        this.videoViewer = videoViewer;
        videoViewer.init(EglContext.get(), null);
    }

    SurfaceViewRenderer getViewer() {
        return videoViewer;
    }
}
