package com.iqbit.qchat.qchat;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;
import org.webrtc.SessionDescription;


public class RTCMessageParser {
    @NonNull
    public static RTCMessageParser create() {
        return new RTCMessageParser();
    }

    public String iceCandidateToJSON(IceCandidate candidate) throws JSONException {
        JSONObject data = new JSONObject();
        data.put("candidate", candidate.sdp);
        data.put("sdpMid", candidate.sdpMid);
        data.put("sdpMLineIndex", candidate.sdpMLineIndex);
        return data.toString();
    }

    public String sdpToJSON(SessionDescription sessionDescription) throws JSONException {
        JSONObject data = new JSONObject();
        data.put("type", sessionDescription.type.canonicalForm());
        data.put("sdp", sessionDescription.description);
        return data.toString();
    }

    public IceCandidate jsonToIceCandidate(String json) throws JSONException {
        JSONObject data = new JSONObject(json);
        return new IceCandidate(
                data.getString("sdpMid"),
                data.getInt("sdpMLineIndex"),
                data.getString("candidate")
        );
    }

    public SessionDescription jsonToSessionDescription(String json) throws JSONException {
        JSONObject data = new JSONObject(json);
        return new SessionDescription(
                SessionDescription.Type.fromCanonicalForm(data.getString("type")),
                data.getString("sdp")
        );
    }
}
