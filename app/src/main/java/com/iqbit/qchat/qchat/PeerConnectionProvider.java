package com.iqbit.qchat.qchat;

import android.content.Context;

import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;


import java.util.ArrayList;

public class PeerConnectionProvider {
    private PeerConnectionFactory peerConnectionFactory;
    private PeerConnection peerConnection;

    PeerConnectionProvider(Context context, PeerConnection.Observer observer) {
        ArrayList<PeerConnection.IceServer> iceServers = new ArrayList<>();
        iceServers.add(PeerConnection.IceServer.builder("stun:stun.stunprotocol.org:3478").createIceServer());
        iceServers.add(PeerConnection.IceServer.builder("stun:stun.l.google.com:19302").createIceServer());

        peerConnectionFactory = createPeerConnectionFactory(context);
        peerConnection = peerConnectionFactory.createPeerConnection(iceServers, observer);
    }

    private PeerConnectionFactory createPeerConnectionFactory(Context context) {
        PeerConnectionFactory.InitializationOptions.Builder optionBuilder = PeerConnectionFactory.InitializationOptions.builder(context);
        optionBuilder.setEnableInternalTracer(true);
        optionBuilder.setFieldTrials("WebRTC-FlexFEC-03/Enabled/");
        PeerConnectionFactory.initialize(optionBuilder.createInitializationOptions());

        PeerConnectionFactory.Builder builder = PeerConnectionFactory.builder();

        return builder.createPeerConnectionFactory();
    }

    public PeerConnection getPeerConnection() {
        return peerConnection;
    }

    public PeerConnectionFactory getPeerConnectionFactory() {
        return peerConnectionFactory;
    }
}
