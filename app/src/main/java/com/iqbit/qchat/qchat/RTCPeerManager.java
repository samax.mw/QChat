package com.iqbit.qchat.qchat;


import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.jetbrains.annotations.Contract;
import org.json.JSONObject;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;


public class RTCPeerManager {
    public interface MediaStreamProvider {
        MediaStream getStream(PeerConnectionFactory factory);
    }

    public interface MediaStreamHandler {
        void onMediaStream(MediaStream stream);
    }

    private PeerConnectionProvider peerProvider = null;
    private PeerConnection peerConnection = null; // makes peerConnection access handy

    private SignalingServerConnection serverConnection;

    RTCPeerManager(SignalingServerConnection serverConnection) {
        this.serverConnection = serverConnection;
    }

    @NonNull
    @Contract(pure = true)
    private PeerConnection.Observer createPeerConnectionObserver(
        String candidateReceiver,
        Context context,
        SignalingServerConnection serverConnection,
        MediaStreamHandler mediaStreamHandler
    ) {
        return new PeerConnection.Observer() {
            @Override
            public void onSignalingChange(PeerConnection.SignalingState signalingState) {

            }

            @Override
            public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {

            }

            @Override
            public void onIceConnectionReceivingChange(boolean b) {

            }

            @Override
            public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {

            }

            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                JSONObject message = new JSONObject();
                try {
                    message.put("type", "candidate");
                    message.put(
                        "data", (new RTCMessageParser()).iceCandidateToJSON(iceCandidate)
                    );
                    message.put("receiver", candidateReceiver);
                } catch (Exception ex) { Log.d("json_error", ex.getMessage()); }
                serverConnection.sendText(message.toString());
            }

            @Override
            public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {

            }

            @Override
            public void onAddStream(MediaStream mediaStream) {
                mediaStreamHandler.onMediaStream(mediaStream);
            }

            @Override
            public void onRemoveStream(MediaStream mediaStream) {

            }

            @Override
            public void onDataChannel(DataChannel dataChannel) {

            }

            @Override
            public void onRenegotiationNeeded() {

            }

            @Override
            public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {

            }
        };
    }

    public void makeOffer(
        String offerReceiver,
        Context context,
        MediaStreamProvider mediaStreamProvider,
        MediaStreamHandler mediaStreamHandler
    ) {
        if (peerProvider != null) return;
        else peerProvider = new PeerConnectionProvider(context, createPeerConnectionObserver(
            offerReceiver, context, serverConnection, mediaStreamHandler
        ));

        peerConnection = peerProvider.getPeerConnection();
        peerConnection.addStream(
            mediaStreamProvider.getStream(peerProvider.getPeerConnectionFactory())
        );

        MediaConstraints constraints = new MediaConstraints();
        constraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveAudio", "true"));
        constraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo", "true"));

        peerConnection.createOffer(new SdpObserver() {
            SessionDescription offerSdp = null;
            String errorTag = "make_offer_error";

            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {
                peerConnection.setLocalDescription(this, sessionDescription);
                offerSdp = sessionDescription;
            }

            @Override
            public void onSetSuccess() {
                JSONObject message = new JSONObject();
                try {
                    message.put("type", "offer");
                    message.put(
                        "data", (new RTCMessageParser()).sdpToJSON(offerSdp)
                    );
                    message.put("receiver", offerReceiver);
                } catch (Exception ex) { Log.d("json_exception", ex.getMessage() + " : 171"); }
                serverConnection.sendText(message.toString());
            }

            @Override
            public void onCreateFailure(String s) { Log.d(errorTag, s); }

            @Override
            public void onSetFailure(String s) { Log.d(errorTag, s); }
        }, constraints);
    }

    public void onIceCandidate(IceCandidate candidate) {
        peerConnection.addIceCandidate(candidate);
    }

    public void onAnswer(SessionDescription answerSdp) {
        peerConnection.setRemoteDescription(new SdpObserver() {
            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {

            }

            @Override
            public void onSetSuccess() {

            }

            @Override
            public void onCreateFailure(String s) {

            }

            @Override
            public void onSetFailure(String s) {

            }
        }, answerSdp);
    }

    public void takeOffer(
        String offerSender,
        SessionDescription offerSdp,
        Context context,
        MediaStreamProvider mediaStreamProvider,
        MediaStreamHandler mediaStreamHandler
    ) {
        if (peerProvider != null) return;
        else peerProvider = new PeerConnectionProvider(context, createPeerConnectionObserver(
            offerSender, context, serverConnection, mediaStreamHandler
        ));

        peerConnection = peerProvider.getPeerConnection();
        peerConnection.addStream(
            mediaStreamProvider.getStream(peerProvider.getPeerConnectionFactory())
        );

        peerConnection.setRemoteDescription(new SdpObserver() {
            int SETTING_RECEIVED_SDP = 0;
            int SETTING_CREATED_SDP = 1;
            int sdpSetState = SETTING_RECEIVED_SDP;

            SessionDescription answerSdp = null;
            String errorTag = "on_offer_error";

            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {
                answerSdp = sessionDescription;
                sdpSetState = SETTING_CREATED_SDP;
                peerConnection.setLocalDescription(this, sessionDescription);
            }

            @Override
            public void onSetSuccess() {
                MediaConstraints constraints = new MediaConstraints();
                constraints.optional.add(new MediaConstraints.KeyValuePair("offerToReceiveAudio", "true"));
                constraints.optional.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo", "true"));

                if (sdpSetState == SETTING_RECEIVED_SDP) peerConnection.createAnswer(
                    this, constraints
                );

                if (sdpSetState == SETTING_CREATED_SDP) {Log.d("onOffer", "sending answer sdp");
                    JSONObject message = new JSONObject();
                    try {
                        message.put("type", "answer");
                        message.put("data", (new RTCMessageParser()).sdpToJSON(answerSdp));
                        message.put("receiver", offerSender);
                    } catch (Exception ex) { Log.d("json_exception", ex.getMessage() + " 251"); }
                    serverConnection.sendText(message.toString());
                }
            }

            @Override
            public void onCreateFailure(String s) { Log.d(errorTag, s); }

            @Override
            public void onSetFailure(String s) { Log.d(errorTag, s); }
        }, offerSdp);
    }
}
