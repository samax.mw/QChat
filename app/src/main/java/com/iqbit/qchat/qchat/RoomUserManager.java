package com.iqbit.qchat.qchat;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;


class RoomUserManager {
    private final ArrayAdapter<String> userListAdapter;
    private HashSet<String> users;
    private String selectedUser;

    private Activity activity;


    RoomUserManager(Activity activity, ListView userListView) {
        this.activity = activity;

        users = new HashSet<>();
        userListAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, new ArrayList<String>());

        userListView.setAdapter(userListAdapter);
        userListView.setOnItemClickListener((AdapterView<?> adapterView, View view, int position, long l) -> {
            setSelectedUser((String)adapterView.getItemAtPosition(position));
        });
    }

    private void setSelectedUser(String user) {
        selectedUser = user;
    }

    public String getSelectedUser() {
        return selectedUser;
    }

    public void addUser(String user) {
        if (!users.add(user)) return;
        Log.d("user_online", user);

        activity.runOnUiThread(() -> {
            userListAdapter.add(user);
            userListAdapter.notifyDataSetChanged();
        });
    }

    public void removeUser(String user) {
        if (!users.remove(user)) return;
        Log.d("user_offline", user);

        activity.runOnUiThread(() -> {
            userListAdapter.remove(user);
            userListAdapter.notifyDataSetChanged();
        });
    }
}
