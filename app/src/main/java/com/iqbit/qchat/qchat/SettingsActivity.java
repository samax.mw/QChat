package com.iqbit.qchat.qchat;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


public class SettingsActivity extends AppCompatActivity {
    private DeviceInfoProvider deviceInfoProvider;
    private SignalingServerURIProvider serverURIProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Settings");
        } else Log.d("SettingsActivity", "actionbar missing.");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        deviceInfoProvider = new DeviceInfoProvider(this);
        serverURIProvider = new SignalingServerURIProvider(this);

        loadSettings();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void applySettings(View view) {
        saveSettings();
    }

    private void loadSettings() {
        String serverAddress = serverURIProvider.getServerURI();
        String deviceId = deviceInfoProvider.getDeviceID();

        ((EditText)findViewById(R.id.serverURIEditor)).setText(serverAddress);
        ((EditText)findViewById(R.id.deviceIdEditor)).setText(deviceId);
    }

    private void saveSettings() {
        String serverURI = ((EditText)findViewById(R.id.serverURIEditor)).getText().toString();
        String deviceId = ((EditText)findViewById(R.id.deviceIdEditor)).getText().toString();

        serverURIProvider.setServerURI(serverURI);
        deviceInfoProvider.setDeviceID(deviceId);
    }
}
