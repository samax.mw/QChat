package com.iqbit.qchat.qchat;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONObject;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SurfaceViewRenderer;


public class MainActivity extends AppCompatActivity {
    private DeviceInfoProvider deviceInfoProvider;

    private SignalingServerURIProvider serverURIProvider;

    private SignalingServerConnection serverConnection;

    private RoomUserManager roomUserManager;

    private RTCPeerManager peerManager;



    RTCMessageParser parser = new RTCMessageParser();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.main_menu, menu);

        menu.getItem(0).setOnMenuItemClickListener((MenuItem settings) -> {
            Intent settingIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingIntent);
            return true;
        });

        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deviceInfoProvider = new DeviceInfoProvider(this);
        serverURIProvider = new SignalingServerURIProvider(this);

        roomUserManager = new RoomUserManager(
            this, (ListView)findViewById(R.id.userListView)
        );


        serverConnection = new SignalingServerConnection(
            "ws://" + serverURIProvider.getServerURI() + "/" + deviceInfoProvider.getDeviceID()
        );


        peerManager = new RTCPeerManager(serverConnection);

        serverConnection.addOnTextMessageListener((socket, text) -> {
            try {
                JSONObject message = new JSONObject(text);

                String type = message.getString("type");
                String data = message.getString("data");

                String sender = "";
                try { sender = message.getString("sender"); }
                catch (Exception ex) {
                    Log.d("json_exception", ex.getMessage() + " " + type + " 112");
                }

                switch (type) {
                    case "deviceOnline":
                        roomUserManager.addUser(data);
                        break;

                    case "deviceOffline":
                        roomUserManager.removeUser(data);
                        break;

                    case "candidate":
                        peerManager.onIceCandidate(parser.jsonToIceCandidate(data));
                        break;

                    case "offer":
                        setSpeakerphoneOn(true);
                        peerManager.takeOffer(
                            sender, parser.jsonToSessionDescription(data),
                            this,
                            this::getCameraStream,
                            this::handleRemoteStream
                        );
                        break;

                    case "answer":
                        peerManager.onAnswer(parser.jsonToSessionDescription(data));
                        break;

                    default:
                        break;
                }
            } catch (Exception ex) {
                Log.d("SigMsg_Error", " : " + ex.getMessage());
            }
        });

        serverConnection.connect((websocket, headers) -> {
            Log.d("MainActivity", "Connected to Signaling Server.");
        }, (websocket, cause) -> {
            Log.d("MainActivity", "Failed to connect to Signaling Server. " + cause.getMessage());
        });
    }

    public void setSpeakerphoneOn(boolean toOn) {
        AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (manager == null) return;
        manager.setSpeakerphoneOn(toOn);
        manager.setMode(AudioManager.MODE_IN_COMMUNICATION);
    }

    public void makeOffer(View view) {
        setSpeakerphoneOn(true);
        peerManager.makeOffer(
            roomUserManager.getSelectedUser(),
            this,
            this::getCameraStream,
            this::handleRemoteStream
        );
    }

    MediaStream getCameraStream(PeerConnectionFactory factory) {
        CamStreamProvider streamProvider = new CamStreamProvider(getApplicationContext());
        MediaStream stream = streamProvider.getCamStream(factory);

        runOnUiThread(() ->{
            VideoViewer videoViewer = new VideoViewer(
                (SurfaceViewRenderer)findViewById(R.id.localSurfaceView)
            );
            stream.videoTracks.get(0).addSink(videoViewer.getViewer());
        });

        return stream;
    }

    void handleRemoteStream(MediaStream stream) {
        AudioManager manager = (AudioManager) getSystemService(
            Context.AUDIO_SERVICE
        );
        if (manager != null) manager.setSpeakerphoneOn(true);

        runOnUiThread(() -> {
            VideoViewer videoViewer = new VideoViewer(
                (SurfaceViewRenderer)findViewById(R.id.remoteSurfaceView)
            );
            stream.videoTracks.get(0).addSink(videoViewer.getViewer());
        });
    }
}
