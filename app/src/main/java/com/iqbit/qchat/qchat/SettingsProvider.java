package com.iqbit.qchat.qchat;

import android.content.Context;
import android.content.SharedPreferences;


public class SettingsProvider {
    private static final String prefName = "SettingsProviderBasic";
    private static final String prefKeyPrefix = "Up0A_";

    private SharedPreferences preferences;

    SettingsProvider(Context context) {
        this.preferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    public String getPrefValue(String key, String defaultValue) {
        return preferences.getString(prefKeyPrefix + key, defaultValue);
    }

    public String getPrefValue(String key) {
        return getPrefValue(key, "");
    }

    public void setPrefValue(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(prefKeyPrefix + key, value);
        editor.apply();
    }
}
