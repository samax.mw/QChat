package com.iqbit.qchat.qchat;

import android.content.Context;

public class DeviceInfoProvider extends SettingsProvider {
    public static final String KEY_DEVICE_ID = "DeviceID";

    DeviceInfoProvider(Context context) {
        super(context);
    }

    public String getDeviceID() {
        return getPrefValue(KEY_DEVICE_ID);
    }

    public void setDeviceID(String address) {
        setPrefValue(KEY_DEVICE_ID, address);
    }
}
