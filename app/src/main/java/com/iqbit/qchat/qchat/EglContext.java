package com.iqbit.qchat.qchat;

import org.webrtc.EglBase;


public class EglContext {
    private static EglBase.Context context = EglBase.create().getEglBaseContext();

    public static EglBase.Context get() {
        return context;
    }
}
